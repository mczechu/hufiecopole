<?php

namespace AppBundle\Utils\Document;

class Type
{
    const ROZKAZ = 'rozkaz';
    const DOKUMENTY = 'dokumenty';
}
