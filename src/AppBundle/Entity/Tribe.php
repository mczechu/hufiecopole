<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tribe
 *
 * @ORM\Table(name="tribe")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TribeRepository")
 */
class Tribe
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=130, unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Troop", mappedBy="tribe")
     */
    private $troops;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tribe
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->troops = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add troop
     *
     * @param \AppBundle\Entity\Troop $troop
     *
     * @return Tribe
     */
    public function addTroop(\AppBundle\Entity\Troop $troop)
    {
        $this->troops[] = $troop;

        return $this;
    }

    /**
     * Remove troop
     *
     * @param \AppBundle\Entity\Troop $troop
     */
    public function removeTroop(\AppBundle\Entity\Troop $troop)
    {
        $this->troops->removeElement($troop);
    }

    /**
     * Get troops
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTroops()
    {
        return $this->troops;
    }
}
