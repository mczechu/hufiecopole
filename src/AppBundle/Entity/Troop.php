<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Troop
 *
 * @ORM\Table(name="troop")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TroopRepository")
 */
class Troop
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="leader", type="string", length=100)
     */
    private $leader;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=255)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="meeting_time", type="string", length=255)
     */
    private $meetingTime;

    /**
     * @ORM\ManyToOne(targetEntity="Tribe",inversedBy="troops")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $tribe;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Troop
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set leader
     *
     * @param string $leader
     *
     * @return Troop
     */
    public function setLeader($leader)
    {
        $this->leader = $leader;

        return $this;
    }

    /**
     * Get leader
     *
     * @return string
     */
    public function getLeader()
    {
        return $this->leader;
    }

    /**
     * Set contact
     *
     * @param string $contact
     *
     * @return Troop
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set meetingTime
     *
     * @param string $meetingTime
     *
     * @return Troop
     */
    public function setMeetingTime($meetingTime)
    {
        $this->meetingTime = $meetingTime;

        return $this;
    }

    /**
     * Get meetingTime
     *
     * @return string
     */
    public function getMeetingTime()
    {
        return $this->meetingTime;
    }

    /**
     * Set tribe
     *
     * @param \AppBundle\Entity\Tribe $tribe
     *
     * @return Troop
     */
    public function setTribe(\AppBundle\Entity\Tribe $tribe = null)
    {
        $this->tribe = $tribe;

        return $this;
    }

    /**
     * Get tribe
     *
     * @return \AppBundle\Entity\Tribe
     */
    public function getTribe()
    {
        return $this->tribe;
    }
}
