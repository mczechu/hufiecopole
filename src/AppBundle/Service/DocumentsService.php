<?php

namespace AppBundle\Service;

use AppBundle\Repository\DocumentRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;

class DocumentsService
{
    /**
     * 
     * @var DocumentRepository
     */
    private $documentRepository;

    public function __construct(Registry $doctrine)
    {
        $this->documentRepository = $doctrine->getRepository('AppBundle:Document');
    }

    public function loadDocument($documentId)
    {
        $document = $this->documentRepository->find($documentId);
        if (!is_object($document)) {
            return false;
        }
        return $document;
    }

    public function getDocuments($type = null)
    {
        if ($type !== null) {
            $documents = $this->documentRepository->findBy(['type' => $type]);
        } else {
            $documents = $this->documentRepository->findAll();
        }

        return $documents;
    }
}
