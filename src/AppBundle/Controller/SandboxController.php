<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SandboxController extends Controller
{

    /**
     * @Route("/sandbox/testmail", name="mailTest")
     */
    public function indexAction(Request $request)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Document');
        $document = new \AppBundle\Entity\Document();

        $document->setCreatedAt(new \DateTime())
            ->setExtension('pdf')
            ->setFilename('Opis faktur jednostronny PDF')
            ->setPath('data/documents/opis-faktur-pdf.pdf')
            ->setType('dokumenty');
        $this->getDoctrine()->getManager()->persist($document);
        $document = new \AppBundle\Entity\Document();
        $document->setCreatedAt(new \DateTime())
            ->setExtension('pdf')
            ->setFilename('Opis faktur jednostronny DOC')
            ->setPath('data/documents/opis-faktur-doc.doc')
            ->setType('dokumenty');
        $this->getDoctrine()->getManager()->persist($document);
        $document = new \AppBundle\Entity\Document();
        $document->setCreatedAt(new \DateTime())
            ->setExtension('pdf')
            ->setFilename('Rozliczenie zaliczki')
            ->setPath('data/documents/rozliczenie-zaliczki.pdf')
            ->setType('dokumenty');
        $this->getDoctrine()->getManager()->persist($document);
        $document = new \AppBundle\Entity\Document();
        $document->setCreatedAt(new \DateTime())
            ->setExtension('pdf')
            ->setFilename('Wniosek o zaliczkę')
            ->setPath('data/documents/wniosek-zaliczka.pdf')
            ->setType('dokumenty');
        $this->getDoctrine()->getManager()->persist($document);
        $this->getDoctrine()->getManager()->flush();
die;
        $message = \Swift_Message::newInstance()
            ->setSubject('Przeczytaj ot')
            ->setFrom('info@3ech.pl')
            ->setTo('mcnatix@gmail.com')
            ->setBody('asdfasdfasdfasdf fajowo');
        $result = $this->get('mailer')->send($message);

//        
//        $obj = \Swift_SmtpTransport::newInstance('3ech.pl', 587)
//            ->setUsername('info@3ech.pl')
//            ->setPassword('48EWfY11fj');
//        $mailer = new \Swift_Mailer($obj);
//        $result = $mailer->send($message);
        /*
         * If you also want to include a plaintext version of the message
          ->addPart(
          $this->renderView(
          'Emails/registration.txt.twig',
          array('name' => $name)
          ),
          'text/plain'
          )
         */
        ;
        $mailCollector = $this->get('mailer');
        $collectedMessages = $mailCollector->getMessages();
        $message = $collectedMessages[0];
        dump($collectedMessages);
        dump($this->get('mailer'));
        dump($result);
        die;
    }

    /**
     * @Route("/sandbox/atestmail", name="mailaTest")
     */
    public function dupaAction()
    {

        $client = static::createClient();
        $crawler = $client->request('POST', '/path/to/above/action');

        $mailCollector = $client->getProfile()->getCollector('swiftmailer');

        // Check that an e-mail was sent
        $this->assertEquals(1, $mailCollector->getMessageCount());

        $collectedMessages = $mailCollector->getMessages();
        $message = $collectedMessages[0];

        // Asserting e-mail data
        $this->assertInstanceOf('Swift_Message', $message);
        $this->assertEquals('Hello Email', $message->getSubject());
        $this->assertEquals('send@example.com', key($message->getFrom()));
        $this->assertEquals('recipient@example.com', key($message->getTo()));
        $this->assertEquals(
            'You should see me from the profiler!', $message->getBody()
        );
        die;
    }
}
