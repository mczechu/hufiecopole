$(document).ready(function () {
    $('.dropdown-submenu').on('click', function (event) {
        event.stopPropagation();
        var ulElement = $(this).find('ul');
        if (!ulElement.is(':visible')) {
            ulElement.show();
        } else {
            ulElement.hide();
        }
    });
    $('body').on('click', 'button.navbar-toggle', function () {
        if ($('.navbar-nav').is(':visible')) {
            $('#closeMenu').hide();
        } else {
            if ($(window).width() < 750)
            {
                $('#closeMenu').show();
            }
        }
    });
    $('body').on('click', '#closeMenu', function () {
        $('button.navbar-toggle').click();
        $('#closeMenu').hide();
    });
//    $('.dropdown-submenu a').on('click', function (event) {
//The event won't be propagated to the document NODE and 
// therefore events delegated to document won't be fired
//        event.stopPropagation();
//    });
    var checkWindowWidth = function () {
        if ($(window).width() >= 750) {
            $('#content').addClass('marginTop50');
            $('#navbarMenu').not('.navbar-fixed-top').addClass("navbar-fixed-top");
        } else {
            $('#content').removeClass('marginTop50');
            $('#navbarMenu').removeClass("navbar-fixed-top");
        }
    };
    $(window).resize(function () {
        checkWindowWidth();
    });
    checkWindowWidth();
// scroll up
    $('.slideUp').click(() => {
        $('html,body').animate({scrollTop: 0}, 'slow');
        return false;
    });
});