<?php

namespace AppBundle\Controller;

use AppBundle\Utils\Document\Type;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use const DIRECTORY_SEPARATOR;
use function dump;
use function file_get_contents;

class GuestController extends Controller
{

    /**
     * @Route("/", name="welcome")
     */
    public function indexAction(Request $request)
    {
        $pageId = $request->get('page_id');
        if ($pageId == '37')
            $this->redirectToRoute('troops');

        $page = $request->get('page');
        $articlesRepository = $this->getDoctrine()->getRepository('AppBundle:Article');
        $articles = $articlesRepository->getByPage($page);
        $maxArticlesAvailable = $articlesRepository->getArticleGroupsCount();
        return $this->render('guest/index.html.twig', [
                'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
                'articles' => $articles,
                'articleGroupsCount' => $maxArticlesAvailable,
        ]);
    }

    /**
     * @Route("/komenda-hufca", name="komenda_hufca")
     */
    public function komendaHufcaAction()
    {
        return $this->render('guest/komenda-hufca.html.twig');
    }

    /**
     * @Route("/jednostki", name="troops")
     */
    public function jednostkiAction()
    {
        $tribesRepository = $this->getDoctrine()->getRepository('AppBundle:Tribe');
        $tribes = $tribesRepository->getTribesSorted();
        return $this->render('guest/jednostki.html.twig', ['tribes' => $tribes]);
    }

    /**
     * @Route("/dokumenty", name="documents")
     */
    public function documentsAction()
    {
        $service = $this->get('app.documents_service');
        $documents = $service->getDocuments(Type::DOKUMENTY);
        return $this->render('guest/dokumenty.html.twig', ['documents' => $documents]);
    }
    /**
     * @Route("/kalendarz", name="calendar")
     */
    public function calendarAction()
    {
        return $this->render('guest/calendar.html.twig');
    }

    /**
     * @Route("/rozkazy", name="rozkazy")
     */
    public function rozkazyAction()
    {
        $service = $this->get('app.documents_service');
        $documents = $service->getDocuments(Type::ROZKAZ);
        return $this->render('guest/rozkazy.html.twig', ['documents' => $documents]);
    }

    /**
     * @Route("/document/{uuid}", name="download-document")
     */
    public function downloadDocumentAction(Request $request)
    {
        $uuid = $request->get('uuid');
        $documentsService = $this->get('app.documents_service');
        $document = $documentsService->loadDocument($uuid);
        if (!is_object($document)) {
            return new Response('Nie znaleziono dokumentu');
        }

        $filePath = $this->get('kernel')->getRootDir() . '/../' . $document->getPath();
        if (!is_file($filePath))
            return new Response('Nie znaleziono dokumentu');

        $content = file_get_contents($filePath);

        $response = new Response();

        //set headers
        $response->headers->set('Content-Type', 'mime/type');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $document->getFilename() . '.' . $document->getExtension());

        $response->setContent($content);
        return $response;
    }

    /**
     * @Route("/jeden-procent-dla-zhp", name="onePercent")
     */
    public function onePercentAction()
    {
        return $this->render('guest/komenda-hufca.html.twig');
    }

    /**
     * @Route("/dane-hufca", name="dane_hufca")
     */
    public function daneHufcaAction()
    {
        return $this->render('guest/dane-hufca.html.twig');
    }

    /**
     * @Route("/aleksander-kaminski", name="bohater_hufca")
     */
    public function bohaterHufcaAction()
    {
        return $this->render('guest/aleksander-kaminski.html.twig');
    }

    /**
     * @Route("/kontakt", name="kontakt")
     */
    public function contactAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $mailTo = $request->get('email');
            $topic = $request->get('topic');
            $content = $request->get('emailContent') . 'od: ' . $mailTo;

            $message = Swift_Message::newInstance()
                ->setSubject('Formularz Kontaktowy: ' . $topic)
                ->setFrom(['info@3ech.pl' => 'Hufiec Opole'])
                ->setTo('marcin@3ech.pl')
                ->setBody($content);
            $result = $this->get('mailer')->send($message);
        }
        return $this->render('guest/kontakt-form.html.twig');
    }
}
