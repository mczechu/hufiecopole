<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\ArticleType;
use AppBundle\Entity\Article;

class AdminController extends Controller
{

    /**
     * @Route("/admin/new-article", name="newArticle")
     */
    public function indexAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sessionUser = $this->get('security.token_storage')->getToken()->getUser();
            $article->setCreatedAt(new \DateTime());
            $article->setCreatedBy($sessionUser);
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();
            return $this->redirectToRoute('userPanel');
        }
        $form->remove('createdAt')->remove('createdBy');
        return $this->render('admin/new-article.html.twig', [
                'form' => $form->createView(),
        ]);
    }
}
